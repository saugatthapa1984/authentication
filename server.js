require('dotenv').config();
const express = require("express");
const app = express();
const jwt = require('jsonwebtoken')
const bodyParser = require("body-parser");
const mongoose = require('mongoose');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require("swagger-ui-express");
const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: "Authentication Api",
            description: "User Authentication Using Jwt",
            contact: {
                name: "Authentication Api using jwt"
            },
            servers: ["http://localhost:3000"]
        }
    },
    apis: ["server.js"],
}


const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use('/auth', swaggerUi.serve, swaggerUi.setup(swaggerDocs));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

mongoose.connect('mongodb://mongo:27017/docker_user', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

var userSchema = new mongoose.Schema({
    username: String,
    password: String,
    email: String
})

var user = mongoose.model('Users', userSchema);
app.use(express.json())
/**
 * @swagger
 *
 * /login:
 *   post:
 *     description: Login to the application
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: username
 *         description: Username to use for login.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: User's password.
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: login
 */
app.post("/login", (req, res, next) => {
    console.log(req.body.username + " username");
    console.log(req.body.password + " password");
    console.log(req.body.email + " email");
    user.findOne({
            username: req.body.username
        })
        .exec()
        .then(userInf => {
            if (userInf == null) {
                return res.status(401).json({
                    message: "Auth failed"
                })
            }
            if (req.body.password === userInf.password) {
                console.log(userInf.username + "email");
                console.log(userInf.email + "usernmae");
                const token = jwt.sign({
                    username: userInf.username,
                    userId: userInf._id,
                }, process.env.ACCESS_TOKEN_SECRET, {
                    expiresIn: "1h"
                });
                return res.status(200).json({
                    message: "Authentication Successful",
                    token: token
                })
            }
            res.status(401).json({
                message: "Auth failed"
            })
        })
})

/**
 * @swagger
 *
 * /register:
 *   post:
 *     description: Login to the application
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: username
 *         description: Username to use for login.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: User's password.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: email
 *         description: users email address
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: login
 */

app.post("/register", function (req, res) {
    const username = req.body.username;
    const password = req.body.password;
    const email = req.body.email;
    user.findOne({
            email: email
        })
        .exec()
        .then(userData => {
            if (userData == null) {
                var newUser = new user({
                    username: username,
                    password: password,
                    email: email
                });
                newUser.save().then(result => {
                    res.status(201).json({
                        message: "User created"
                    })
                })
            } else {
                return res.status(409).json({
                    message: "Mail already exists"
                })
                console.log(userData);
            }
        }).
    catch(
        err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        }
    );
})

/**
 * @swagger
 *
 * /editUser:
 *   post:
 *     description: Login to the application
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: username
 *         description: Username to use for login.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: enter users password
 *         in: formData
 *         required: true
 *         type: string
 *       - name: email
 *         description: users email address.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: newEmail
 *         description: enter new email address
 *         in: formData
 *         required: false
 *         type: string  
 *       - name: newPassword
 *         description: enter new password
 *         in: formData
 *         required: false
 *         type: string  
 *       - name: Authorization
 *         description: user token.
 *         in: header
 *         required: false
 *         type: api_key
 *     responses:
 *       200:
 *         description: login
 */

app.post("/editUser", authenticateToken, function (req, res) {
    const oldUsername = req.body.username;
    const password = req.body.password;
    const email = req.body.email;
    const newPassword = req.body.newPassword;
    const newEmail = req.body.newEmail;
    console.log(req.userData);
    console.log(oldUsername + " old username");
    if (req.userData.username !== oldUsername) {
        res.status(500).json({
            error: "Authentication error"
        });
    } else {
        user.findOne({
                username: req.body.username
            })
            .exec()
            .then(userInf => {
                if (userInf == null) {
                    return res.status(401).json({
                        message: "Auth failed"
                    })
                }
                if (password === userInf.password) {
                    userInf.email = newEmail;
                    userInf.password = newPassword;
                    userInf.save();
                    res.send("User info has been sucessfully updated")
                } else {
                    res.send("Enter the correct password");
                }
            })
    }
})

/**
 * @swagger
 *
 * /deleteUser:
 *   post:
 *     description: delete a user
 *     produces:
 *       - application/json
 *       - Authorization 
 *     parameters:
 *       - name: username
 *         description: users username.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: users password
 *         in: formData
 *         required: true
 *         type: string
 *       - name: Authorization
 *         description: enter value
 *         in: header
 *         required: false
 *         type: api_key
 *     responses:
 *       200:
 *         description: Delete existing user
 */

app.post("/deleteUser", authenticateToken, function (req, res) {
    const userName = req.body.username;
    if (req.userData.username !== userName) {
        res.status(500).json({
            error: "Authentication error"
        });
    } else {
        user.findOne({
                username: userName
            })
            .exec()
            .then(userInf => {
                if (userInf == null) {
                    return res.status(401).json({
                        message: "Auth failed"
                    })
                }
                if (req.body.password === userInf.password) {
                    userInf.deleteOne();
                    res.send("user sucessfully deleted");
                } else {
                    res.send("Cannot authenticate")
                }
            })
    }
})

function authenticateToken(req, res, next) {
    try {
        const token = req.headers.authorization.split(" ")[1];
        const decoded = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET)
        req.userData = decoded;
        next();
    } catch (error) {
        return res.status(401).json({
            message: "Auth failed"
        });
    }
    // console.log(req.userData);
}


app.listen(3000, function (req, res) {
    console.log("Server has sucessfully started");
})