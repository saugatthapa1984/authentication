# Jwt  Authentication
### Running Jwt Authentication
* Installation
  * $ npm i mongoose jsonwebtoken express dotenv swagger-jsdoc 
 
### pre-requisites
* mongodb and node is locally installed
    * node and npm installation
* secret key
    *  To create a secret key and encrypt user information using it a .env file will have to be created with a vaiable called ACCESS_TOKEN_SECRET it can be assigned any value the user wants
    * for example
        * ACCESS_TOKEN_SECRET=abc 

### Node.js and mongodb installation  
* $ pacman -S nodejs npm
* mongodb installation
    * link: https://docs.mongodb.com/manual/administration/install-on-linux/
* mongodb arch linux installation
    * link: https://wiki.archlinux.org/index.php/MongoDB
 ---
## Run the server locally using node
* $ npm run devStart 
---
### Run the server using docker
- $ docker-compose up.
- $ docker-compose up -d (for detached mode).
---

### open the swagger-ui in browser
* http://localhost:3000/auth/#/
---

### Usage
* Below is a detalied documentation of how to install and run the Jwt authentication api. That was     made using node

---

## Routes
### Login
* The login endpoint returns a unqiue token to the user if the user is already registered in the database.
    * http://localhost:3000/auth/#/default/post_login
```
{
	"username":"hari",
	"password":"1234"
}
```

---

### Register

* The register endpoint registers the user to the database if the user is not already present in the database
    * http://localhost:3000/auth/#/default/post_register

```
A valid sample
{
	"username":"saugat",
	"password":"12345",
	"email":"saugat@gmail.com"
}
```

---
### Edit
* The edit endpoint allows the user to edit thier information after providing a valid email address and token id.
    * http://localhost:3000/auth/#/default/post_editUser

```
A valid smaple
{
	"username":"saugat",
	"password":"12345",
	"email":"saugat@gmail.com",
	"newEmail": "shyam",
	"newPassword":"123"
}
```




---

### Delete
* The edit endpoint allows the user to delete thier information after providing a valid email address and token id.
    * http://localhost:3000/auth/#/default/post_editUser
```
A valid sample
{ 

	"username":"saugat",
	"password":"1234"
	
}
```
---

### Methods used
#### A function which works as a middle-ware for all the routes has been created to check if the token is recieved and the user is valid
```
function authenticateToken(req, res, next) {
    try {
        const token = req.headers.authorization.split(" ")[1];
        const decoded = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET)
        req.userData = decoded;
        next();
    } catch (error) {
        return res.status(401).json({
            message: "Auth failed"
        });
    }
}
```
---









